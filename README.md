# OtomeGameProject

A small collaboration of sorts. Details to follow.

# Repository Structure

```
README.md               This file
THIRDPARTY.md           Lists external assets used
game/                   Ren'Py project folder
    gui/                
    assets.rpy          Declares images, sounds, and other files used
    characters.rpy      Declares characters used in the game
    defines.rpy         Global objects (transforms)
    gui.rpy             UI configuration
    options.rpy         Project settings
    screens.rpy         Screen (Styles)
    script.rpy          Main entry point
```

# Building the Project

**Prerequisites**
- Ren'Py 6.99.11.1749

# LICENSE
Copyright 2016 Fractor Cor

A list of third party assets and the license under which they are included can
be found in `THIRDPARTY.md`.
