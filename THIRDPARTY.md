A list of third party assets included in this project can be found at the links
below.

Asset                     | License
--------------------------|----------------------------------------------------|
Ren'Py                    | [MIT](Renpy)

[Renpy]:https://www.renpy.org/doc/html/license.html
