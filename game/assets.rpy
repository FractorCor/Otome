# Declare assets (e.g. images, sounds) used by this game.

init -1:
    # Images
    image logo fractorcor = "gui/logo.png"

    image black = Solid((0, 0, 0, 255))
    image white = Solid((255, 255, 255, 255))
    image grey  = Solid((128, 128, 128, 255))
