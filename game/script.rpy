﻿# You can place the script of your game in this file.

# Scripting Conventions
# - no lines longer than 80 columns
# - named individuals are to use their first name (+ last init, if conflict)
# - non-descript characters are to use a shortened name, like 'n' for narration

# Scripting Guidelines
# - apply transitions when rearranging or introducing another sprite
#   - this could also apply for changing stances or facial expressions
#       - don't change poses/facial expresions until their next speaking moment
# - apply screenwipes to provide sense of direction when switching locations

label splashscreen:
    scene white
    with Pause(1)

    show logo fractorcor at truecenter
    with dissolve
    with Pause(2)

    scene black with dissolve
    with Pause(1)

    return

## The game starts here.

label start:

    ## Show a background. This uses a placeholder by default, but you can add a
    ## file (named either "bg room.png" or "bg room.jpg") to the images
    ## directory to show it.

    scene bg room

    ## This shows a character sprite. A placeholder is used, but you can replace
    ## it by adding a file named "eileen happy.png" to the images directory.

    show eileen happy

    ## These display lines of dialogue.

    "Hello, world."

    e "You've created a new Ren'Py game."

    e "Once you add a story, pictures, and music, you can release it to the world!"

    ## This ends the game.

    return
