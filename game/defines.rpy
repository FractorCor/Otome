# Declare values (e.g. transforms, transitions) used by this game.

init -1:

    # Commonly Used Transforms
    # - often used for character placement
    # - RenPy Documentation
    #   - Style Props https://www.renpy.org/doc/html/style_properties.html
    #   - Transforms  https://www.renpy.org/doc/html/transforms.html
    #   - ATL Scripts https://www.renpy.org/doc/html/atl.html#atl
    transform center_left:
        xanchor 0.5
        yalign 1.0
        xpos 0.3

    transform center_right:
        xanchor 0.5
        yalign 1.0
        xpos 0.7

    # Commonly Used Transitions
    # - often used to change scenes
    # - RenPy Documentation
    #   - Pre-Defined https://www.renpy.org/doc/html/transitions.html
